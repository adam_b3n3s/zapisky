# Latex zápisky

Nejprve jsem psal zápisky do tabletu pak v Latexu. Asi holt nemám kapacitu vše to přepisovat. začátek je v Writu, (který je zbytečný dávat na Gitlab). Zbytek v Latexu.

## Co jsem přepsal (nepřepsal) do Latexu

Vše jsem přepsal (ne úplně, chybý Diskrétka a Lingebra 1).

## Upozornění

Kompletní zápisky **NE** skripta, pokud mě psát si zápisky z nějakého předmětu nebaví, zkrátka je nepíšu, (obvykle se jedná o předměty jako programování, kde je to zbytečné nebo o předměty, kde učitel pouze čte slidy. Slidy se opravdu blbě opisují a ztrácí to smysl, když má člověk stejně k dispozici slidy).

# Latex notes

First, I wrote notes on a tablet, then in Latex. I guess I don't have the capacity to rewrite it all. the beginning is in Write, (which is useless to put on Gitlab). The rest in Latex.

## What I rewrote (didn't rewrote) in Latex

I rewrote everything (not completely, missing Discrete math and Linear algebra 1).

## Warning

Complete notes **NOT** the scripts, if I don't enjoy writing notes from a subject, I simply don't write them, (usually these are subjects like programming, where it is unnecessary or subjects where the teacher only reads the slides. The slides are really stupid to rewrite and it loses its meaning when one has slides at his disposal anyway).
